const ctrlAuth = require('../controllers/authentication');

const ctrlProfile = require('../controllers/profile');



const express = require('express');

const router = express.Router();

const jwt = require('express-jwt');



const auth = jwt({

  secret: 'MY_SECRET',

  userProperty: 'payload',

  algorithms: ['HS256']

});




// profile

router.get('/profile', auth, ctrlProfile.profileRead);

router.post('/register', auth, ctrlAuth.register);



// authentication


router.post('/login', ctrlAuth.login);



module.exports = router;